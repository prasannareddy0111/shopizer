# Keypair
resource "aws_key_pair" "keypair" {
  key_name   = "keypair"
  public_key = "${file("/root/test/keypair.pub")}"
}

### Instance
resource "aws_instance" "pubic_instance" {
  ami           = "ami-00e782930f1c3dbc7"
  instance_type = "${var.instance_type}"
  key_name      = "${aws_key_pair.keypair.key_name}"

  vpc_security_group_ids = ["${aws_security_group.web-sg.id}"]
  subnet_id              = "${element(aws_subnet.public_subnet.*.id, 1)}"
  associate_public_ip_address = true
   tags {
    Name = "Public_Instance"
  }
}
