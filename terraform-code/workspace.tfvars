public_subnet_cidr_block  = "10.0.1.0/24"
vpc_cidr_block = "10.0.0.0/22"
aws_region = "ap-south-1"
availability_zones = ["ap-south-1a", "ap-south-b"]
keypair =  "/root/test/keypair.pub"
instance_type = "t2.micro"
