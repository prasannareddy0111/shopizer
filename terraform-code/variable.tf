variable "keypair" {
   description = "EC2 instance public key"
}

variable "public_subnet_cidr_block" {
   description = "Public Subnet CIDR Block"
}

variable "aws_region" {
    description = "AWS Region"
}
variable "vpc_cidr_block" {
    description = "Main VPC CIDR Block"
}

variable "availability_zones" {
   type = "list"
   description = "AWS Region Availability Zones"
}

variable "instance_type" {
   description = "Instance Type"
}

